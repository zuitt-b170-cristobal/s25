//Aggregation in MongoDB
	//aggregate - cluster of things that have come 
	//aggregation operations process data records & return computed values; group values from multiple docs together
	//aggregations are needed when your app needs a form of info that's not visibly available from your doc



	db.fruits.insertMany({
		    name: "apple",
		    color: "red",
		    stock: 1000,
		    price: 150,
		    supplier_id: 1,
		    onSale: false,
		    origin: [
		      "Korea", "Japan"
		    ]
		},
		{
		    name: "mango",
		    color: "yellow",
		    stock: 1000,
		    price: 250,
		    supplier_id: 2,
		    onSale: false,
		    origin: [
		      "Philippines", "Thailand"
		    ]
		},
		{
		    name: "banana",
		    color: "yellow",
		    stock: 1000,
		    price: 100,
		    supplier_id: 3,
		    onSale: false,
		    origin: [
		      "Philippines", "Thailand"
		    ]
		},
		{
		    name: "orange",
		    color: "orange",
		    stock: 1000,
		    price: 160,
		    supplier_id: 4,
		    onSale: false,
		    origin: [
		      "Korea", "Japan"
		    ]
		}
		);

//to find items on sale
db.fruits.find({onSale: true})



db.fruits.aggregate([
    {$match: {onSale: true}}
])

db.fruits.updateOne(
    {
        supplier_id: 3
    },
    {
        $set: {
            supplier_id: 1
        }
    }
);

db.fruits.updateOne(
    {
        supplier_id: 4
    },
    {
        $set: {
            supplier_id: 2
        }
    }
);


db.fruits.updateOne(
    {
        name: "mango"
    },
    {
        $set: {
            onSale: true
        }
    }
);


db.fruits.aggregate([
    {$match: {onSale: false}}
]);

//code works from top to bottom
	//result will add total stock of all supplier_id with same code
db.fruits.aggregate([
    {$match: {onSale: false}}, //similar to find()
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}
    //trying to set the fields to appear without affecting/changing the data; ex. $sum: "of stocks from same supplier codes"
    //id: is grouped w value of $supplier_id while total is grouped by summation of $stock;
]);


db.fruits.aggregate([
    {$match: {onSale: false}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$project: {_id: 0}} //will filter out supplier_id; value of 1 or 0 only; default for project
]);



db.fruits.aggregate([
    {$match: {onSale: false}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total: 1}} //arranging value of field given. ex. for this, sorted from least to greatest value
]);


db.fruits.aggregate([
    {$match: {onSale: false}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total: -1}} //sorted from greatest to least value
]);


//unwind operator - for deconstructing array field by filtering each origin array element to be listed per document
db.fruits.aggregate([
    {$unwind: "$origin"}
]);


db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id:"$origin", kinds:{$sum:1}}}
]);
	//grouped by origin, to show kinds of fruits coming per origin
	//"$sum: 1" refer to how many document has a field whose value is a certain country
	/*
	{
    "_id" : "Korea",
    "kinds" : 2.0
	}
	*/ 

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$match: {origin: "China"}},
    {$group: {_id:"$origin", kinds:{$sum:1}}}
]);
//if you only want to see china


====================NORMALIZED DATA
//reference data


var owner = ObjectId()

db.owners.insert({
		    _id: owner,
		    name: "John",
		    contact: "091712345678",
		    
		});
//will create new document in the collection owners


//1 is to 1 relationship
db.suppliers.insert({
		    name: "ABC Fruits",
		    contact: "091712345678",
                    owner_id: ObjectId("6255757861c01d61dd59039a")
		});

//owner id means the id of this owner John
/*
{
    "_id" : ObjectId("6255763761c01d61dd59039b"),
    "name" : "ABC Fruits",
    "contact" : "091712345678",
    "owner_id" : ObjectId("6255757861c01d61dd59039a")
}
*/
	//means that John's object Id will be the owner of abc fruits by copying object id of db.owners.insert...name: John
	//schema? 1 supplierName has a relationship to 1 owner

========DENORMALIZED DATA
	//Address is embedded in suppliers collection instead of having address of all suppliers

//1 is to few relationship
	db.suppliers.insert({
    name:"DEF Fruits",
    contact:"09987654321",
    address:[
        {street: "123 San Jose St.", city: "Manila"},
        {street: "367 Gil Puyat", city:"Makati"}
    ]
});

========NORMALIZED DATA
	//reference data

var supplier = ObjectId()
var branch1 = ObjectId()
var branch2 = ObjectId()

db.suppliers.insert({
    _id: supplier,
    name:"GHI Fruits",
    contact:"09456789123",
    branches:[ branch1, branch2]
});
/*======RESULT:=======
{
    "_id" : ObjectId("625579a561c01d61dd59039d"),
    "name" : "GHI Fruits",
    "contact" : "09456789123",
    "branches" : [ 
        ObjectId("625579a561c01d61dd59039e"), 
        ObjectId("625579a561c01d61dd59039f")
    ]
}
*/




db.branches.insertMany([{
    _id: ObjectId("625579a561c01d61dd59039e"),
    name: "BF Homes",
    address: "123 Arcadia Santos St.",
    city: "paranaque",
    supplier_id: ObjectId("625579a561c01d61dd59039d") 
    },
    {
    _id: ObjectId("625579a561c01d61dd59039f")
    name: "Rizal",
    address: "123 San Jose St.",
    city: "Manila",
    supplier_id: ObjectId("625579a561c01d61dd59039d")
        }
    ]);

    /* RESULT:
===== 1 =====
	{
    "_id" : ObjectId("625579a561c01d61dd59039e"),
    "name" : "BF Homes",
    "address" : "123 Arcadia Santos St.",
    "city" : "paranaque",
    "supplier_id" : ObjectId("625579a561c01d61dd59039d")
}

=== 2 ====
{
    "_id" : ObjectId("625579a561c01d61dd59039f"), <branchId>
    "name" : "Rizal",
    "address" : "123 San Jose St.",
    "city" : "Manila",
    "supplier_id" : ObjectId("625579a561c01d61dd59039d") <supplierId>
}
*/

//Normalized data - instead of having long data, you reference info from another data/document



